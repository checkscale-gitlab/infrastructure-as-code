locals {
  aws_region = "ap-southeast-1"

  tags = merge({
    Region = local.aws_region
  }, var.tags)
}

data "aws_caller_identity" "current" {}

provider "aws" {
  version = "~> 2.52"
  region  = local.aws_region
}



