variable "tags" {
  type        = map(string)
  default     = {}
  description = "A mapping of tags to assign to all the resources created by this module."
}
