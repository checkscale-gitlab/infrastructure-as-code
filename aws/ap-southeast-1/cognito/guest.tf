# resource "aws_cognito_user_group" "guest" {
#   name         = "user-group"
#   user_pool_id = aws_cognito_user_pool.default.id
#   description  = "Guest users"
#   precedence   = 42
#   role_arn     = aws_iam_role.guest-authenticated.arn
# }

# resource "aws_iam_role" "guest-authenticated" {
#   name               = "guest-authenticated"
#   path               = "/cognito/"
#   assume_role_policy = data.aws_iam_policy_document.guest-authenticated-assume.json
# }

# data "aws_iam_policy_document" guest-authenticated-assume {
#   statement {
#     effect  = "Allow"
#     actions = ["sts:AssumeRoleWithWebIdentity"]
#     principals {
#       identifiers = ["cognito-identity.amazonaws.com"]
#       type        = "Federated"
#     }
#     condition {
#       test     = "StringEquals"
#       variable = "cognito-identity.amazonaws.com:aud"
#       values   = [aws_cognito_identity_pool.guest.id]
#     }
#     condition {
#       test     = "ForAnyValue:StringLike"
#       variable = "cognito-identity.amazonaws.com:amr"
#       values   = ["authenticated"]
#     }
#   }
# }

# resource "aws_iam_role_policy" "guest-authenticated" {
#   name   = "guest-authenticated"
#   role   = aws_iam_role.guest-authenticated.id
#   policy = data.aws_iam_policy_document.guest-authenticated.json
# }

# data "aws_iam_policy_document" guest-authenticated {
#   statement {
#     sid    = "CognitoSync"
#     effect = "Allow"
#     actions = [
#       "cognito-sync:*",
#     ]
#     resources = [
#       "arn:aws:cognito-sync:${local.aws_region}:${data.aws_caller_identity.current.account_id}:identitypool/${aws_cognito_identity_pool.guest.id}",
#     ]
#   }

#   statement {
#     sid       = "LakeFormation"
#     effect    = "Allow"
#     actions   = ["lakeformation:GetDataAccess"]
#     resources = ["*"]
#   }

#   statement {
#     sid    = "AthenaQuery"
#     effect = "Allow"
#     actions = [
#       "athena:BatchGetQueryExecution",
#       "athena:CancelQueryExecution",
#       "athena:GetCatalogs",
#       "athena:GetExecutionEngine",
#       "athena:GetExecutionEngines",
#       "athena:GetNamespace",
#       "athena:GetNamespaces",
#       "athena:GetQueryExecution",
#       "athena:GetQueryExecutions",
#       "athena:GetQueryResults",
#       "athena:GetQueryResultsStream",
#       "athena:GetTable",
#       "athena:GetTables",
#       "athena:ListQueryExecutions",
#       "athena:RunQuery",
#       "athena:StartQueryExecution",
#       "athena:StopQueryExecution",
#       "athena:ListWorkGroups",
#       "athena:GetWorkGroup"
#     ]
#     resources = ["*"]
#   }

#   statement {
#     sid    = "AthenaGlue"
#     effect = "Allow"
#     actions = [
#       # "glue:CreateDatabase",
#       # "glue:DeleteDatabase",
#       "glue:GetDatabase",
#       "glue:GetDatabases",
#       # "glue:UpdateDatabase",
#       # "glue:CreateTable",
#       # "glue:DeleteTable",
#       # "glue:BatchDeleteTable",
#       # "glue:UpdateTable",
#       "glue:GetTable",
#       "glue:GetTables",
#       # "glue:BatchCreatePartition",
#       # "glue:CreatePartition",
#       # "glue:DeletePartition",
#       # "glue:BatchDeletePartition",
#       # "glue:UpdatePartition",
#       "glue:GetPartition",
#       "glue:GetPartitions",
#       "glue:BatchGetPartition"
#     ]
#     resources = ["*"]
#   }

#   statement {
#     sid    = "ReadS3"
#     effect = "Allow"
#     actions = [
#       "s3:GetBucketLocation",
#       "s3:GetObject",
#       "s3:ListBucket",
#       "s3:GetBucketAcl",
#     ]
#     resources = [
#       "arn:aws:s3:::e2fyi-athena",
#       "arn:aws:s3:::e2fyi-athena/*",
#     ]
#   }

#   statement {
#     sid    = "WriteS3"
#     effect = "Allow"
#     actions = [
#       "s3:PutObject",
#       "s3:DeleteObject",
#     ]
#     resources = [
#       "arn:aws:s3:::e2fyi-athena/outputs/guest/*",
#     ]
#   }

# }

