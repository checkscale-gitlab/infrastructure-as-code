resource "aws_cognito_user_pool" "default" {
  name = "default"

  username_attributes        = ["email"]
  email_verification_subject = "[paypaw.co] Verification Email"

  admin_create_user_config {
    allow_admin_create_user_only = true
  }

  mfa_configuration = "OPTIONAL"
  software_token_mfa_configuration {
    enabled = true
  }

}

resource "aws_cognito_user_pool_domain" "demo" {
  domain       = "e2fyi"
  user_pool_id = aws_cognito_user_pool.default.id
}

resource "aws_cognito_user_pool_client" "demo" {
  name                                 = "demo"
  user_pool_id                         = aws_cognito_user_pool.default.id
  allowed_oauth_flows                  = ["implicit", "code"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes                 = ["email", "openid", "profile"]

  supported_identity_providers = [
    aws_cognito_identity_provider.google.provider_name
  ]

  callback_urls = [
    "https://paypaw.co",
    "https://e2fyi.auth.ap-southeast-1.amazoncognito.com/oauth2/idpresponse"
  ]
  default_redirect_uri = "https://e2fyi.auth.ap-southeast-1.amazoncognito.com/oauth2/idpresponse"
}
