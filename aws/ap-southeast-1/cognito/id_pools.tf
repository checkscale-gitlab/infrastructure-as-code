
resource "aws_cognito_identity_pool" "guest" {
  identity_pool_name               = "guest"
  developer_provider_name          = "guest"
  allow_unauthenticated_identities = false

  supported_login_providers = {
    "accounts.google.com" = var.google_provider_client_id
  }

  tags = merge({ Name : "guest" }, var.tags)
}

# resource "aws_cognito_identity_pool_roles_attachment" "main" {
#   identity_pool_id = aws_cognito_identity_pool.guest.id

#   role_mapping {
#     identity_provider         = aws_cognito_identity_provider.google.id
#     ambiguous_role_resolution = "AuthenticatedRole"
#     type                      = "Rules"

#     mapping_rule {
#       claim      = "isAdmin"
#       match_type = "Equals"
#       role_arn   = aws_iam_role.guest-authenticated.arn
#       value      = "paid"
#     }
#   }

#   roles = {
#     "authenticated" = "${aws_iam_role.authenticated.arn}"
#   }
# }

