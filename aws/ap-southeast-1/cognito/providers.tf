variable google_provider_client_id {
  type        = string
  description = "Client id for Google cognito provider"
}

variable google_provider_client_secret {
  type        = string
  description = "Client secret for Google cognito provider"
}

resource "aws_cognito_identity_provider" "google" {
  user_pool_id  = aws_cognito_user_pool.default.id
  provider_name = "Google"
  provider_type = "Google"

  provider_details = {
    authorize_scopes = "profile email openid"
    client_id        = var.google_provider_client_id
    client_secret    = var.google_provider_client_secret
  }
  attribute_mapping = {
    email    = "email"
    username = "sub"
    name     = "name"
  }
}
