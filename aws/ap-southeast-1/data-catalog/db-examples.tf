################################################################################
# Each database should have its own tf file or dir (if there are going to be
# many tables inside the db)
################################################################################
resource "aws_glue_catalog_database" "examples" {
  name        = "examples"
  description = "An example database to be used with Athena and backed by S3"
}

################################################################################
# You can explicitly declare the schema of ur data (i.e. table)
################################################################################
# resource "aws_glue_catalog_table" "examples-iris" {
#   name          = "iris"
#   database_name = aws_glue_catalog_database.examples.name
# }

################################################################################
# Glue job to update data catalog with CSV data inside s3://e2fyi-athena/csv
# NOTE: Each csv file MUST be inside its own dir (as multiple CSV are considered
# partitions)
################################################################################
resource "aws_glue_crawler" "csv" {
  database_name = aws_glue_catalog_database.examples.name
  name          = "csv"
  description   = "AWS Glue crawler to sync table with S3 path"
  role          = aws_iam_role.glue-s3-ro.arn

  # https://docs.aws.amazon.com/glue/latest/dg/monitor-data-warehouse-schedule.html
  # schedule = "cron(0 0 1 * ? *)" # every first day of the month

  table_prefix = "csv_"

  s3_target {
    path = "s3://e2fyi-athena/csv/"
  }

  schema_change_policy {
    delete_behavior = "DELETE_FROM_DATABASE"
    update_behavior = "UPDATE_IN_DATABASE"
  }

  tags = merge({ Name : "csv-crawler" }, var.tags)
}

################################################################################
# Glue can also create a table from a set of NDJson or raw JSON files.
# This glue job create a sarcasm_headlines table from 2 ndjson files, and 1 json
# file, all located inside s3://e2fyi-athena/json/sarcasm_headlines/
################################################################################
resource "aws_glue_crawler" "sarcasm-headlines" {
  database_name = aws_glue_catalog_database.examples.name
  name          = "sarcasm_headlines"
  description   = "AWS Glue crawler to sync table with S3 path"
  role          = aws_iam_role.glue-s3-ro.arn

  # https://docs.aws.amazon.com/glue/latest/dg/monitor-data-warehouse-schedule.html
  # schedule = "cron(0 0 1 * ? *)" # every first day of the month

  s3_target {
    path = "s3://e2fyi-athena/json/sarcasm_headlines/"
  }

  schema_change_policy {
    delete_behavior = "DELETE_FROM_DATABASE"
    update_behavior = "UPDATE_IN_DATABASE"
  }

  tags = merge({ Name : "sarcasm-headlines-crawler" }, var.tags)

  configuration = <<EOF
{
  "Version":1.0,
  "Grouping": {
    "TableGroupingPolicy": "CombineCompatibleSchemas"
  }
}
EOF
}
