resource "aws_athena_workgroup" "guest" {
  name = "guest"

  configuration {
    enforce_workgroup_configuration    = true
    publish_cloudwatch_metrics_enabled = false

    result_configuration {
      output_location = "s3://e2fyi-athena/outputs/guest/"

      encryption_configuration {
        encryption_option = "SSE_S3"
      }
    }
  }
}
