################################################################################
# Assume role policy
################################################################################
data aws_iam_policy_document "glue-assume-role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["glue.amazonaws.com"]
      type        = "Service"
    }
  }
}

data aws_iam_policy_document "athena-glue" {

  statement {
    sid    = "Glue"
    effect = "Allow"
    actions = [
      "glue:CreateDatabase",
      "glue:DeleteDatabase",
      "glue:GetDatabase",
      "glue:GetDatabases",
      "glue:UpdateDatabase",
      "glue:CreateTable",
      "glue:DeleteTable",
      "glue:BatchDeleteTable",
      "glue:UpdateTable",
      "glue:GetTable",
      "glue:GetTables",
      "glue:BatchCreatePartition",
      "glue:CreatePartition",
      "glue:DeletePartition",
      "glue:BatchDeletePartition",
      "glue:UpdatePartition",
      "glue:GetPartition",
      "glue:GetPartitions",
      "glue:BatchGetPartition",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "Athena"
    effect = "Allow"
    actions = [
      "athena:*",
      "lakeformation:GetDataAccess",
    ]
    resources = ["*"]

    condition {
      test     = "Bool"
      variable = "aws:ViaAWSService"

      values = [
        "false",
      ]
    }

    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"

      values = [
        "138.75.7.184",
      ]
    }
  }

  statement {
    sid       = "PassRole"
    effect    = "Allow"
    actions   = ["iam:PassRole"]
    resources = ["arn:aws:iam::*:role/glue/*"]
    condition {
      test     = "StringLike"
      variable = "iam:PassedToService"
      values   = ["ec2.amazonaws.com", "glue.amazonaws.com"]
    }
  }

  statement {
    sid    = "Logs"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:*:*:/aws-glue/*"]
  }
}

################################################################################
# aws glue read-only IAM
################################################################################
resource "aws_iam_role" "glue-s3-ro" {
  name               = "glue-s3-ro"
  path               = "/glue/"
  assume_role_policy = data.aws_iam_policy_document.glue-assume-role.json
}

resource "aws_iam_role_policy_attachment" "glue-s3-ro" {
  role       = aws_iam_role.glue-s3-ro.name
  policy_arn = aws_iam_policy.glue-s3-ro.arn
}

resource "aws_iam_policy" "glue-s3-ro" {
  name        = "glue-s3-ro"
  path        = "/glue/"
  description = "iam policy for aws glue to s3:ro"
  policy      = data.aws_iam_policy_document.glue-s3-ro.json
}

data aws_iam_policy_document "glue-s3-ro" {

  source_json = data.aws_iam_policy_document.athena-glue.json

  statement {
    sid    = "ListS3"
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "ReadS3"
    effect = "Allow"
    actions = [
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:GetBucketAcl",
    ]
    resources = [
      "arn:aws:s3:::e2fyi-athena",
      "arn:aws:s3:::e2fyi-athena/*",
    ]
  }
}

################################################################################
# aws glue read-write IAM
################################################################################
resource "aws_iam_role" "glue-s3-rw" {
  name               = "glue-s3-rw"
  path               = "/glue/"
  assume_role_policy = data.aws_iam_policy_document.glue-assume-role.json
}

resource "aws_iam_role_policy_attachment" "glue-s3-rw" {
  role       = aws_iam_role.glue-s3-rw.name
  policy_arn = aws_iam_policy.glue-s3-rw.arn
}

resource "aws_iam_policy" "glue-s3-rw" {
  name        = "glue-s3-rw"
  path        = "/glue/"
  description = "iam policy for aws glue to s3:rw"
  policy      = data.aws_iam_policy_document.glue-s3-rw.json
}

data aws_iam_policy_document "glue-s3-rw" {

  source_json = data.aws_iam_policy_document.glue-s3-ro.json

  statement {
    sid    = "WriteS3"
    effect = "Allow"
    actions = [
      "s3:PutObject",
      "s3:DeleteObject",
    ]
    resources = [
      "arn:aws:s3:::e2fyi-athena/*",
    ]
  }
}
