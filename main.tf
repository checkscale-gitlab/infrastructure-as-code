terraform {
  backend "s3" {
    bucket = "e2fyi-vault"
    key    = "terraform/terraform.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  version = "~> 2.52"
  region  = "ap-southeast-1"
}

locals {
  tags = {
    ManagedBy = "terraform"
    Repo      = "https://gitlab.com/e2fyi/infrastructure-as-code"
  }
}

module "aws-ap-southeast-1" {
  source = "./aws/ap-southeast-1"
  tags   = local.tags

  gitlab_runner_terraform_token = var.gitlab_runner_terraform_token
}

module "data-catalog" {
  source = "./aws/ap-southeast-1/data-catalog"
  tags   = local.tags
}

module "cognito" {
  source = "./aws/ap-southeast-1/cognito"
  tags   = local.tags

  google_provider_client_id     = var.google_provider_client_id
  google_provider_client_secret = var.google_provider_client_secret
}
